/* aNetHack 0.0.1	qt_kde0.h	$ANH-Date: 1432512779 2015/05/25 00:12:59 $  $ANH-Branch: master $:$ANH-Revision: 1.8 $ */
/* Copyright (c) Warwick Allison, 1999. */
/* aNetHack may be freely redistributed.  See license for details. */

#ifndef QT_DUMMYKDE
#define QT_DUMMYKDE
class KTopLevelWidget : public QMainWindow
{
    Q_OBJECT
};
#endif
