/* aNetHack 0.0.1	winGnome.h	$ANH-Date: 1432512782 2015/05/25 00:13:02 $  $ANH-Branch: master $:$ANH-Revision: 1.8 $ */
/* Copyright (C) 1998 by Erik Andersen <andersee@debian.org> */
/* Copyright (C) 1998 by Anthony Taylor <tonyt@ptialaska.net> */
/* aNetHack may be freely redistributed.  See license for details. */

#ifndef WINGNOME_H
#define WINGNOME_H

#define E extern

E struct window_procs Gnome_procs;

#undef E

#define NHW_WORN 6
extern winid WIN_WORN;

#endif /* WINGNOME_H */
