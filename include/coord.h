/* aNetHack 0.0.1	coord.h	$ANH-Date: 1432512778 2015/05/25 00:12:58 $  $ANH-Branch: master $:$ANH-Revision: 1.9 $ */
/* Copyright (c) Stichting Mathematisch Centrum, Amsterdam, 1985. */
/* aNetHack may be freely redistributed.  See license for details. */

#ifndef COORD_H
#define COORD_H

typedef struct nhcoord {
    xchar x, y;
} coord;

#endif /* COORD_H */
