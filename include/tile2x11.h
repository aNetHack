/* aNetHack 0.0.1	tile2x11.h	$ANH-Date: 1432512778 2015/05/25 00:12:58 $  $ANH-Branch: master $:$ANH-Revision: 1.9 $ */
/* aNetHack may be freely redistributed.  See license for details. */

#ifndef TILE2X11_H
#define TILE2X11_H

/*
 * Header for the x11 tile map.
 */
typedef struct {
    unsigned long version;
    unsigned long ncolors;
    unsigned long tile_width;
    unsigned long tile_height;
    unsigned long ntiles;
    unsigned long per_row;
} x11_header;

/* how wide each row in the tile file is, in tiles */
#define TILES_PER_ROW (40)

#endif /* TILE2X11_H */
