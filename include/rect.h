/* aNetHack 0.0.1	rect.h	$ANH-Date: 1432512778 2015/05/25 00:12:58 $  $ANH-Branch: master $:$ANH-Revision: 1.8 $ */
/* Copyright (c) 1990 by Jean-Christophe Collet			  */
/* aNetHack may be freely redistributed.  See license for details. */

#ifndef RECT_H
#define RECT_H

typedef struct nhrect {
    xchar lx, ly;
    xchar hx, hy;
} NhRect;

#endif /* RECT_H */
