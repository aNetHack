/* aNetHack 0.0.1	gnworn.h	2009/05/06 10:58:06  1.3 */
/*
 * $ANH-Date: 1432512804 2015/05/25 00:13:24 $  $ANH-Branch: master $:$ANH-Revision: 1.8 $
 */
/* Copyright (C) 2002 by Dylan Alex Simon		*/
/* aNetHack may be freely redistributed.  See license for details. */

#ifndef GnomeHackWornWindow_h
#define GnomeHackWornWindow_h

#include <gnome.h>
#include "config.h"
#include "global.h"

GtkWidget *ghack_init_worn_window();

#endif /* GnomeHackWornWindow_h */
