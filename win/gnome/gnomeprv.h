/* aNetHack 0.0.1	gnomeprv.h	$ANH-Date: 1432512806 2015/05/25 00:13:26 $  $ANH-Branch: master $:$ANH-Revision: 1.9 $ */
/* Copyright (C) 1998 by Erik Andersen <andersee@debian.org> */
/* aNetHack may be freely redistributed.  See license for details. */

#ifndef GnomeHack_h
#define GnomeHack_h

/* These are the base anethack include files */
#include "hack.h"
#include "dlb.h"
#include "patchlevel.h"

#include "winGnome.h"

#endif /* GnomeHack_h */
