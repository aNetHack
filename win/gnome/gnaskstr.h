/* aNetHack 0.0.1	gnaskstr.h	$ANH-Date: 1432512806 2015/05/25 00:13:26 $  $ANH-Branch: master $:$ANH-Revision: 1.8 $ */
/* Copyright (C) 1998 by Erik Andersen <andersee@debian.org> */
/* aNetHack may be freely redistributed.  See license for details. */

#ifndef GnomeHackAskStringDialog_h
#define GnomeHackAskStringDialog_h

int ghack_ask_string_dialog(const char *szMessageStr,
                            const char *szDefaultStr, const char *szTitleStr,
                            char *buffer);

#endif /* GnomeHackAskStringDialog_h */
