/* aNetHack 0.0.1	gnopts.h	$ANH-Date: 1432512806 2015/05/25 00:13:26 $  $ANH-Branch: master $:$ANH-Revision: 1.8 $ */
/* Copyright (C) 1998 by Erik Andersen <andersee@debian.org> */
/* aNetHack may be freely redistributed.  See license for details. */

#ifndef GnomeHackSettings_h
#define GnomeHackSettings_h

void ghack_settings_dialog(void);

#endif /* GnomeHackSettings.h */
