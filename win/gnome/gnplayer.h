/* aNetHack 0.0.1	gnplayer.h	$ANH-Date: 1432512807 2015/05/25 00:13:27 $  $ANH-Branch: master $:$ANH-Revision: 1.9 $ */
/* Copyright (C) 1998 by Erik Andersen <andersee@debian.org> */
/* aNetHack may be freely redistributed.  See license for details. */

#ifndef GnomeHackPlayerSelDialog_h
#define GnomeHackPlayerSelDialog_h

int ghack_player_sel_dialog(const char **, const gchar *, const gchar *);

#endif /* GnomeHackPlayerSelDialog_h */
