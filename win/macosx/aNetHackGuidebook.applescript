#!/usr/bin/osascript
# aNetHack 0.0.1  aNetHackGuidebook.applescript $ANH-Date$  $ANH-Branch$:$ANH-Revision$
# Copyright (c) Kenneth Lorber, Kensington, Maryland, 2011
# aNetHack may be freely redistributed.  See license for details.

# Display the Guidebook from the GUI.

tell application "Finder"
        open location "file:///Library/Nethack/doc/aNetHackGuidebook.pdf"
        delay 5
end tell
