#!/usr/bin/osascript
# aNetHack 0.0.1  aNetHackRecover.applescript $ANH-Date$  $ANH-Branch$:$ANH-Revision$
# Copyright (c) Kenneth Lorber, Kensington, Maryland, 2009
# aNetHack may be freely redistributed.  See license for details. 



set canceled to false
try
	display dialog "Welcome to the aNetHack recover program.  Please make sure aNetHack is not running before continuing.  Ready?" with title "aNetHackRecover"
on error number -128
	set canceled to true
end try
if not canceled then
	set hpath to the path to me
	set mpath to the POSIX path of hpath
	considering case
		--set lastpos to offset of "/anethackdir" in mpath
		--set lastpos to lastpos + (length of "/anethackdir")
		--set rawpath to (get text 1 through lastpos of mpath) & "/recover.pl"
		--set safepath to the quoted form of rawpath
		set safepath to the quoted form of "/Library/Nethack/anethackdir/recover.pl"
	end considering
	do shell script safepath
	display dialog result with title "aNetHackRecover Output"
end if
