/* Copyright (C) 2002 by Alex Kompel */
/* aNetHack may be freely redistributed.  See license for details. */
/*
$ANH-Date: 1432512811 2015/05/25 00:13:31 $  $ANH-Branch: master $:$ANH-Revision: 1.6 $
*/

#ifndef MSWINSplashWindow_h
#define MSWINSplashWindow_h

#include "winMS.h"
#include "config.h"
#include "global.h"

void mswin_display_splash_window(BOOL);

#endif /* MSWINSplashWindow_h */
