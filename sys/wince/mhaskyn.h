/* aNetHack 0.0.1	mhaskyn.h	$ANH-Date: 1432512800 2015/05/25 00:13:20 $  $ANH-Branch: master $:$ANH-Revision: 1.8 $ */
/* Copyright (C) 2001 by Alex Kompel 	 */
/* aNetHack may be freely redistributed.  See license for details. */

#ifndef MSWINAskYesNO_h
#define MSWINAskYesNO_h

#include "winMS.h"

int mswin_yes_no_dialog(const char *question, const char *choices, int def);

#endif /* MSWINAskYesNO_h */
