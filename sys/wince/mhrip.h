/* aNetHack 0.0.1	mhrip.h	$ANH-Date: 1432512802 2015/05/25 00:13:22 $  $ANH-Branch: master $:$ANH-Revision: 1.11 $ */
/* Copyright (C) 2001 by Alex Kompel 	 */
/* aNetHack may be freely redistributed.  See license for details. */

#ifndef MSWINRIPWindow_h
#define MSWINRIPWindow_h

#include "winMS.h"
#include "config.h"
#include "global.h"

HWND mswin_init_RIP_window();
void mswin_display_RIP_window(HWND hwnd);

#endif /* MSWINRIPWindow_h */
