/* aNetHack 0.0.1	win32api.h	$ANH-Date: 1432516197 2015/05/25 01:09:57 $  $ANH-Branch: master $:$ANH-Revision: 1.11 $ */
/* Copyright (c) aNetHack PC Development Team 1996                 */
/* aNetHack may be freely redistributed.  See license for details. */

/*
 * This header file is used to clear up some discrepencies with Visual C
 * header files & aNetHack before including windows.h, so all aNetHack
 * files should include "win32api.h" rather than <windows.h>.
 */
#if defined(_MSC_VER)
#undef strcmpi
#undef min
#undef max
#pragma warning(disable : 4142) /* Warning, Benign redefinition of type */
#pragma pack(8)
#endif

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <commctrl.h>

#if defined(_MSC_VER)
#pragma pack()
#endif

/*win32api.h*/
