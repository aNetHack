/* aNetHack 0.0.1	ntsound.c	$ANH-Date: 1432512794 2015/05/25 00:13:14 $  $ANH-Branch: master $:$ANH-Revision: 1.11 $ */
/*   Copyright (c) aNetHack PC Development Team 1993                 */
/*   aNetHack may be freely redistributed.  See license for details. */
/*                                                                  */
/*
 * ntsound.c - Windows NT aNetHack sound support
 *
 *Edit History:
 *     Initial Creation                              93/12/11
 *
 */

#include "hack.h"
#include "win32api.h"
#include <mmsystem.h>

#ifdef USER_SOUNDS

void
play_usersound(filename, volume)
const char *filename;
int volume;
{
    /*    pline("play_usersound: %s (%d).", filename, volume); */
    (void) sndPlaySound(filename, SND_ASYNC | SND_NODEFAULT);
}

#endif /*USER_SOUNDS*/
/* ntsound.c */
